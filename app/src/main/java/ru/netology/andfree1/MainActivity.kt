package ru.netology.andfree1

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.TextView

class MainActivity : AppCompatActivity() {


    val TAG: String = "MainActivity"

    override fun onCreate(savedInstanceState: Bundle?) {
        Log.d(TAG, "start of onCreate function")
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        val name: String = "Ivan"
        val surname: String = "Ivanov"
        var age: Int = 32
        var heigt: Double = 172.2

        age = 33
        age = 34

        age = age + 1
        age += 1
        age++

        val summary: String = " name: $name surname $surname age $age heigt $heigt"

        val output: TextView = findViewById(R.id.output)
        output.text = summary

        Log.d(TAG, summary)
        Log.d(TAG, "end of onCreate function")
    }
}